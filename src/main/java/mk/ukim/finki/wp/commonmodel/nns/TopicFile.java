package mk.ukim.finki.wp.commonmodel.nns;


import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class TopicFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fileName;

    private byte[] content;

    public TopicFile(String fileName, byte[] content) {
        this.fileName = fileName;
        this.content = content;
    }
}
