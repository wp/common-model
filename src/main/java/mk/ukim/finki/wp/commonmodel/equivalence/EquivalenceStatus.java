package mk.ukim.finki.wp.commonmodel.equivalence;

public enum EquivalenceStatus {

    REQUESTED, SUBJECTS_IMPORTED, EQUIVALENCE_IN_PROGRESS, FINISHED
}
