package mk.ukim.finki.wp.commonmodel.attendance;

public enum ClassType {

    LECTURE,
    AUDITORY_EXERCISE,
    LABORATORY
}
