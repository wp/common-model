package mk.ukim.finki.wp.commonmodel.memorandum;

public enum WebsitePostingStatus {
    REQUESTED, APPROVED, REJECTED, PUBLISHED
}
