ALTER TABLE internship
ALTER COLUMN description TYPE TEXT;

ALTER TABLE internship_week
ALTER COLUMN description TYPE TEXT;

ALTER TABLE internship_week
ALTER COLUMN company_comment TYPE TEXT;

ALTER TABLE internship_week
ALTER COLUMN coordinator_comment TYPE TEXT;